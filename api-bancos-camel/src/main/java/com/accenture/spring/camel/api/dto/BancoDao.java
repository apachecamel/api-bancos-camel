package com.accenture.spring.camel.api.dto;

import org.springframework.data.jpa.repository.JpaRepository;

public interface BancoDao extends JpaRepository<Banco, Long> {

}
